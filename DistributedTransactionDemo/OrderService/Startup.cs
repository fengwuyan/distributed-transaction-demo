using EF.Model;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            string mySqlConStr = Configuration["CapSetting:MySqlConStr"];
            string rabbitMqHostStr = Configuration["CapSetting:RabbitMQHostStr"];
            services.AddDbContext<CommonContext>(options =>
            {
                options.UseMySQL(mySqlConStr);
            });

            services.AddCap(opt =>
            {
                opt.UseMySql(mySqlConStr);
                opt.UseRabbitMQ(rabbitMqHostStr);
                opt.FailedRetryCount = 10;
                opt.FailedRetryInterval = 60;
                opt.FailedThresholdCallback = failed =>
                {
                    //TODO:发送邮件，人工处理
                    Console.WriteLine("发送邮件，需人工处理， cap报错：" + failed.Message);
                };
            });
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
