﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Model
{
    /// <summary>
    /// 订单
    /// </summary>
    [Table("Order")]
    public class Order
    {
        /// <summary>
        /// 订单id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 订单状态:0：待支付，1：已支付，等待发货
        /// </summary>
        public int OrderStatus { get; set; }
        /// <summary>
        /// 所属用户
        /// </summary>
        public int UserId { get; set; }
    }
}
