﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Model
{
    public class CommonContext : DbContext
    {

        public CommonContext(DbContextOptions<CommonContext> options) : base(options)
        {
            Console.WriteLine($"This is {nameof(CommonContext)}  DbContextOptions");
        }

        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<Payment> Payment { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }
    }
}
