﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF.Model
{
    [Table("Payment")]
    public class Payment
    {
        /// <summary>
        /// 支付表id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 订单id
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// 支付金额
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// 支付状态：0：待支付，1：已支付
        /// </summary>
        public int ApplyStatus { get; set; }
        /// <summary>
        /// 用户id
        /// </summary>
        public int UserId { get; set; }
    }
}
